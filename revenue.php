<?php
require_once('../rg_dev/ga_revenue.php');  

//echo "<br /><br /><br />funnel by source Array<br />";
//print_r($funnel_by_source);
?>
<!DOCTYPE html>
<html>
<head>
    
    
      <title>

      </title>
    
    <style>
        table {
            margin: 20px;
            padding: 0px;
            border: 1px solid #000;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
            border-top-right-radius: 0px;
            border-top-left-radius: 0px;
            border-collapse: collapse;
            border-spacing: 0;
            /*width: 95%;*/
        }
          
        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }
          
        th {
            background-color: #ccc;
            border: 1px solid #000;
            text-align: center !important;
            
        }
          
        table tr:hover {
            background-color: #ccc !important;
        }
        
        td, th {
            vertical-align: middle;
            border: 1px solid #000;
            border-width: 0px 1px 1px 0px;
            text-align: left;
            padding: 7px;
            font-size: 14px;
            font-family: Arial;
            /*font-weight: normal;*/
            color: #000;
            width: 260px;
        }
        table tr:nth-child(odd) {
           background-color: #e5e5e5;
        }
        table tr:nth-child(even) {
            background-color: #fff;
        }
        h1, h3 {
            text-align: left;

            font-family: "Times New Roman";
        }
        h1 {
            font-size: 2em !important;
        }
        h3 {
            font-size: 1.25em !important;
        }

        .product {
          width: 125px;
        }
          
        .sales {
            width: 70px;
            text-align: center;
        }
        .revenue {
           width: 90px;
        }
        
        .source {
            width: 200px;
          }
          
          .spend {
              width: 90px;
          }
          
          .roas {
              width: 50px;
          }
    </style>
    
</head>
<body>
    <h1>Revenue By Source</h1>
    <table>
        <tbody>
            <tr><th class='source'>Source</th><th class='spend'>Spend</th><th class='revenue'>Revenue</th><th class='roas'>ROAS</th></tr>
            <?php 
                foreach($funnel_by_source as $key=>$revenue_array) {

                    if($key == 'total')
                    {
                        $key = "Total";
                        $revenue = $revenue_array['revenue'];
                    }
                    else {
                        $revenue = $revenue_array['total']['revenue'];
                    }           
                    $revenue = number_format($revenue,2);

                    echo "<tr>";
                        echo "<td class='source'>$key</td>";
                        echo "<td class='spend'>-</td>";
                        echo "<td class='revenue'>$ $revenue</td>";
                        echo "<td class='roas'>-</td>";
                    echo "</tr>";
                }


            ?>
        </tbody>
    </table>
    
    <h1>STC</h1>
    <h3>Total Revenue : $<?php echo $stc['total']['revenue']; ?> </h3>
    <table> 
        <tbody>
            <tr><th class='product'>Product</th><th class='sales'>Sales</th><th class='revenue'>Revenue</th></tr>
            <?php 
            
            foreach($stc['groups'] as $key=>$revenue_array_values) {
                $stc_sales = $revenue_array_values['purchases'];
                $stc_revenue = number_format($revenue_array_values['revenue'],2);
                
                echo "<tr><td class='product'>$key</td><td class='sales'>$stc_sales</td><td class='revenue'>$ $stc_revenue</td></tr>";
            }
            
            ?>
            
        </tbody>
    </table>
    
      <h1>Vayner</h1>
    <h3>Total Revenue : $<?php echo $vayner['total']['revenue']; ?> </h3>
    <table> 
        <tbody>
            <tr><th class='product'>Product</th><th class='sales'>Sales</th><th class='revenue'>Revenue</th></tr>
            <?php 
            
            foreach($vayner['groups'] as $key=>$revenue_array_values) {
                $vayner_sales = $revenue_array_values['purchases'];
                $vayner_revenue = number_format($revenue_array_values['revenue'],2);
                
                echo "<tr><td class='product'>$key</td><td class='sales'>$vayner_sales</td><td class='revenue'>$ $vayner_revenue</td></tr>";
            }
            
            ?>
            
        </tbody>
    </table>
</body>
</html>
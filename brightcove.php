<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Document</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    </head>
    <body>


<div style="width: 80%; margin: 0px auto; text-align: center;" class="BCL_containingBlock">
<div style="display: block; position: relative; max-width: 720px;">
                      <div style="display: block; padding-top: 400px;">
                              <video id="vsl_player"
                                      data-account="3527089191001" 
                                      data-player="a29a19fd-6f9b-48bc-9c2b-32e3b68c28b6" 
                                      data-playlist-id="3601801618001"
                                      data-embed="default" 
                                      class="video-js" 
                                      controls="controls"
                                      style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></video>
                              <script src="//players.brightcove.net/3527089191001/a29a19fd-6f9b-48bc-9c2b-32e3b68c28b6_default/index.min.js"></script>
<script>

var myPlayer,
textTrack;
videojs("vsl_player").ready(function(){
    var myPlayer = this;
    var tt = '';
    var currentCue = '';
    var startTime, endTime, cueName;
    myPlayer.on("firstplay", function () {
        var myObj = myPlayer.mediainfo.cue_points;
        console.log('Mediainfo', myObj);
        textTrack = myPlayer.addTextTrack('metadata', 'Timed Cue Point');
        for(i =0; i < myObj.length; i++) {
            startTime = myObj[i].time;
            endTime = startTime;
            cueName = myObj[i].name;
            textTrack.addCue(new window.VTTCue(startTime,endTime,cueName));
        }   
        
        var allTT = myPlayer.textTracks();
        var i = allTT.length - 1;
        console.log('i:'+i);
        tt = allTT[i];
        tt.oncuechange = function() {
            if(tt.activeCues[0] !== undefined){
                currentCue = tt.activeCues[0];
                onCue(currentCue.text);
                console.log(tt.activeCues[0]);
            }
        };
    });

});

function onCue(theCue) {
console.log(theCue);
    if (theCue !== undefined) {
        //console.log(theCue);
        switch (theCue) {
            case "Show Hidden Page Element":
				bcWorking = true;
                PreventExitSplash = "true";
                $("#pausedNote").hide();
                $("#mainProgressBar").hide();
                $("#buyNow").fadeIn('slow', buyButtonVisibleToggle('buy'));
                if (typeof dataLayer != 'undefined') {
                    dataLayer.push({
                        'event': 'sendVPV',
                        'vpv': window.location.pathname + '/OPP-visible'
                    });
                }
                engagementLevel = 'Finished VSL';
                upsertEl(engagementLevel);
                break;

            case "Product Intro":
				bcWorking = true;
                if (typeof dataLayer != 'undefined') {
                    dataLayer.push({
                        'event': 'sendVPV',
                        'vpv': window.location.pathname + '/VSL-product-intro'
                    });
                }
                engagementLevel = 'Saw Product Intro';
                upsertEl(engagementLevel);
                break;

            case "Pause Activation":
				bcWorking = true;
                $("img").trigger('unveil');
                //console.log("pauseActivated");
                pauseActivated = "true";
                break;

            case "Fix Viewed":
				bcWorking = true;
                engagementLevel = 'Saw Swing Fix';
                upsertEl(engagementLevel);
                break;

        }
    }
}

</script>
                      </div>
              </div> 
</div>

    </body>
</html>